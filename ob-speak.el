;;; ob-speak.el --- org-babel functions for Espeak speech synthesis

;; Copyright (C) 2009-2012  Free Software Foundation, Inc.

;; Authors: Eric Schulte
;;	    David T. O'Toole <dto@gnu.org>
;; Keywords: literate programming, reproducible research, speech synthesis
;; Homepage: http://orgmode.org

;; This file is not (yet) part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Feed a block to espeak for sound output and/or writing to file.

;;; Code:
(require 'ob)
(eval-when-compile (require 'cl))

(defvar org-babel-tangle-lang-exts)
(add-to-list 'org-babel-tangle-lang-exts '("speak" . "speak"))
(defvar org-babel-default-header-args:speak '())
(defvar org-babel-header-args:speak '((package . :any)))

(defcustom org-babel-speak-dir-fmt
  "(let ((*default-pathname-defaults* #P%S)) %%s)"
  "Format string used to wrap code bodies to set the current directory.
For example a value of \"(progn ;; %s\\n   %%s)\" would ignore the
current directory string."
  :group 'org-babel
  :version "24.1"
  :type 'string)

(defun org-babel-expand-body:speak (body params)
  "Expand BODY according to PARAMS, return the expanded body."
  (let* ((vars (mapcar #'cdr (org-babel-get-header params :var)))
	 (result-params (cdr (assoc :result-params params)))
	 (print-level nil) (print-length nil)
	 (body (org-babel-trim
		(if (> (length vars) 0)
		    (concat "(let ("
			    (mapconcat
			     (lambda (var)
			       (format "(%S (quote %S))" (car var) (cdr var)))
			     vars "\n      ")
			    ")\n" body ")")
		  body))))
    (if (or (member "code" result-params)
	    (member "pp" result-params))
	(format "(pprint %s)" body)
      body)))

;;; Now comes the speech part.

(defvar org-babel-speak-program "espeak")

(defvar org-babel-suppress-file-output nil)

;; (defun org-babel-speak-preview ()
;;   (interactive)
;;   (let ((org-babel-suppress-file-output t))
;;     (org-babel

(defvar org-babel-speak-parameters 
  '(:voice "-v"
    :volume "-a"
    :pitch "-p"
    :markup "-m"
    :speed "-s"
    :punctuation "--punct"
    :wav "-w"))

(defvar org-babel-additional-parameters '("--stdin" "-l" "0" "-m"))

(defun org-babel-make-espeak-parameter (entry)
  (destructuring-bind (name . value) entry
    (let ((option (getf org-babel-speak-parameters name)))
      (when option
	(list option value)))))

(defun org-babel-make-espeak-parameters (babel-params)
  (apply #'append (mapcar #'org-babel-make-espeak-parameter
			  babel-params)))

(defvar org-babel-speak-parameter-function 'org-babel-make-espeak-parameters)

(defun org-babel-speak-command-string (params)
  (mapconcat 'identity 
	     (append (list org-babel-speak-program)
		     org-babel-additional-parameters
		     (org-babel-make-espeak-parameters params))
	     " "))

(defun org-babel-execute:speak (body params)
  "Send a block to voice synthesis with Babel."
    (with-temp-buffer
	(insert (org-babel-expand-body:speak body params))
      ;;
      (let ((command-string (org-babel-speak-command-string params)))
	(shell-command-on-region (point-min) (point-max) command-string))))

(provide 'ob-speak)

;;; ob-speak.el ends here
